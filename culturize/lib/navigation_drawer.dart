import 'package:culturize/about_us.dart';
import 'package:culturize/contact_us.dart';
import 'package:culturize/explore.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';


class NavigationDrawerDemo extends StatefulWidget {
  NavigationDrawerDemo() : super();

  final String title = "CULTURIZE";

  @override
  _NavigationDrawerDemoState createState() => _NavigationDrawerDemoState();
}

class _NavigationDrawerDemoState extends State<NavigationDrawerDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset('images/logo.png',
                    width: 500.0,
                    height: 100.0,),
                  SizedBox(height: 15,),
                  /*Text("CULTURIZE",
                    style: TextStyle(color: Colors.black, fontSize: 17.0),)*/
                ],
              ),
              decoration: BoxDecoration(
                color: Colors.pink,
              ),
            ),
            ListTile(
              leading: Icon(Icons.explore_sharp),
              title: Text('Explore', style: TextStyle(color: Colors.black, fontSize: 17.0)),
              onTap: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => Explore()));
              },
            ),
            ListTile(
              leading: Icon(Icons.contact_mail_sharp),
              title: Text('About Us', style: TextStyle(color: Colors.black, fontSize: 17.0)),
              onTap: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => AboutUs()));
              },
            ),
            ListTile(
              leading: Icon(Icons.phone_in_talk),
              title: Text('Contact Us', style: TextStyle(color: Colors.black, fontSize: 17.0)),
              onTap: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => ContactUs()));
              },
            ),

          ],
        ),
      ),
      appBar: AppBar(
        title: new Center(child: Text(
          widget.title,
          style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),),),
      ),
      body: WebView(
        initialUrl: "http://culturize.com/",
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );



  }
}